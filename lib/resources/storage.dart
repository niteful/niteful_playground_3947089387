import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'dart:async';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:local_auth/local_auth.dart';

var flutterLocalNotificationsPlugin = new FlutterLocalNotificationsPlugin();

class Storage {

  // * #############################################
  // * #############################################
  // * #############################################

  // ? Appreciations
  // ? #############################################

  void checkSettings() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    print("SETTINGS " + prefs.getKeys().toString());
  }

  checkNotifications() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getBool("notifications");
  }

  Future<String> getTextFromFile(String shour, String sminute) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    int h = prefs.getInt(shour);
    int m = prefs.getInt(sminute);

    if((h != null)||(m != null)) {
      return h.toString() + "h | " + m.toString() + "m";
    }
    return "Turn notifcations on.";
  }

 Future<bool> setMorningNotificationTime(TimeOfDay t) async {
    if(t != null) {
      SharedPreferences prefs = await SharedPreferences.getInstance();

      print("THOUR: " + t.hour.toString());
      print("TMINUTE: " + t.minute.toString());

      prefs.setInt("morningHour", t.hour);
      prefs.setInt("morningMinute", t.minute);
    }
    return true;
  }

  Future showMorningNotification(String pload) async {

      if(pload == null) {
        print("NOT SETTING MORNING NOTIFICATION: NULL");
        return;
      }

      if(pload.length < 1) {
        print("NOT SETTING MORNING NOTIFICATION: EMPTY");
        return;
      }

      SharedPreferences prefs = await SharedPreferences.getInstance();
      int h = prefs.getInt("morningHour") ?? 8;
      int m = prefs.getInt("morningMinute") ?? 0;

      print("MORNING NOTIFICATION " + pload + " @ HOUR: " + h.toString() + " + MINUTE: " + m.toString());
    
      var cday = new DateTime.now();
      var nday = cday.add(new Duration(days: 1));

      var inputday = new DateTime(cday.year, cday.month, cday.day, h, m);

      var scheduledNotificationDateTime;

      if(cday.isBefore(inputday)) {
        print("SENDING MESSAGE TODAY");
        scheduledNotificationDateTime = inputday;
      } else {
        print("SENDING MESSAGE TOMORROW");
        scheduledNotificationDateTime = new DateTime(nday.year, nday.month, nday.day, h, m);
      }
    

    var androidPlatformChannelSpecifics = new AndroidNotificationDetails(
        'morning-messages',
        'Morning Messages',
        'These are messages people set for themselves in the morning.');
    var iOSPlatformChannelSpecifics =
        new IOSNotificationDetails(sound: "slow_spring_board.aiff");
    var platformChannelSpecifics = new NotificationDetails(
        androidPlatformChannelSpecifics, iOSPlatformChannelSpecifics);
    await flutterLocalNotificationsPlugin.schedule(
        1,
        'Good Morning!',
        pload,
        scheduledNotificationDateTime,
        platformChannelSpecifics,
        payload: "M:" + pload);
  }

 Future<bool> setNightNotificationTime(TimeOfDay t) async {
    if(t != null) {
      SharedPreferences prefs = await SharedPreferences.getInstance();

      print("THOUR: " + t.hour.toString());
      print("TMINUTE: " + t.minute.toString());

      prefs.setInt("nightHour", t.hour);
      prefs.setInt("nightMinute", t.minute);
    }
    scheduleNightNotification();
    return true;
  }

  Future<bool> setInsightNum(int i) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    print("SETTING IN:" + i.toString());
    prefs.setInt("insightNum", i);
    return true;
  }

  Future<int> getInsightNum() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    print("GETTING IN:" + (prefs.getInt("insightNum") ?? -1).toString());
    return prefs.getInt("insightNum") ?? -1;
  }

  Future<bool> setBiometrics() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    var localAuth = LocalAuthentication();
    bool didAuthenticate =
        await localAuth.authenticateWithBiometrics(
            localizedReason: 'Turn On Biometric Check');
    print(didAuthenticate);

    prefs.setBool("biometrics", didAuthenticate);
    return true;
  }

  Future scheduleNightNotification() async {
    
    SharedPreferences prefs = await SharedPreferences.getInstance();
    print(prefs.getInt("nightHour"));
    print(prefs.getInt("nightMinute"));
    int h = prefs.getInt("nightHour") ?? 22;
    int m = prefs.getInt("nightMinute") ?? 0;

    var time = new Time(h, m, 0);
    var androidPlatformChannelSpecifics = new AndroidNotificationDetails(
        'repeatDailyAtTime channel id',
        'repeatDailyAtTime channel name',
        'repeatDailyAtTime description');
    var iOSPlatformChannelSpecifics = new IOSNotificationDetails();
    var platformChannelSpecifics = new NotificationDetails(
        androidPlatformChannelSpecifics, iOSPlatformChannelSpecifics);
    await flutterLocalNotificationsPlugin.showDailyAtTime(
        0,
        "Let's wrap up your day.",
        "Here are a few questions for you.",
        time,
        platformChannelSpecifics,
        payload: "N:ERROR");
  }
}


